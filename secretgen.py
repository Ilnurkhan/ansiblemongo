import string
import random


def generate_password(lenght=12):
    if lenght < 12:
        lenght = 12
    PASS_ATRS = [
        'ascii_lowercase',
        'ascii_uppercase',
        'digits'
        ]
    password = ''.join(
        random.choice(getattr(string, x)) for x in PASS_ATRS
        for y in range(int(lenght/4)) 
        )
    punctuation_len = lenght - (int(lenght/4) * len(PASS_ATRS))
    password += ''.join(
        random.choice(string.punctuation) for x in range(punctuation_len)
        )
    password = list(password)
    random.shuffle(password)
    return ''.join(password)



if __name__ == '__main__':
    admin_pass = generate_password(15)
    diamond_pass = generate_password(15)
    print('admin_password:', admin_pass)
    print('diamond_password:', diamond_pass)
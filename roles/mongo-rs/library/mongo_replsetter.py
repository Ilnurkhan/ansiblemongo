import time

import pymongo
from ansible.module_utils.basic import *

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'h1dw0w'
}

class MongoReplicaset:
    def __init__(self, ansible_module):
        self.is_initiated = True
        self.module = ansible_module
        self.name = self.module.params['replset_name']
        self.nodes = self.str_list_to_list(self.module.params['members'])
        self.login = self.module.params.get('admin_login')
        self.password = self.module.params.get('admin_password')
        self.auth_enabled = self.check_auth_need()
        self.connection = self.__get_connection()

    def check_auth_need(self):
        try:
            with pymongo.MongoClient(self.nodes) as client:
                client.admin.command('replSetGetConfig')
        except Exception as error:
            if 'auth' in str(error).lower():
                return True
            if 'primary' in str(error).lower():
                self.is_initiated = False # mb to sep func?
        else:
            return False

    def __get_connection(self):
        # TODO: http://api.mongodb.com/python/current/examples/authentication.html#mongodb-cr
        connection_params = {'replicaset': self.name}
        if self.auth_enabled:
            connection_params.update(
                {
                'username': self.login,
                'password': self.password,
                }
            )
        try:
            return pymongo.MongoClient(self.nodes, **connection_params)
        except Exception as Error:
            self.module.fail_json(
                    msg='Replicaset reconfig failed: {}'.format(Error)
                    )

    
    def sync_config(self):
        '''
        Syncing Ansible replSet conf and Mongo rs.cfg
        '''
        if self.is_initiated:
            config = self.connection.admin.command('replSetGetConfig')['config']
            max_id = max(config['members'], key=lambda x: x.get('_id'))['_id']
            current_hosts = {str(x.get('host')) for x in config['members']}
            any_change = False
            for host in self.nodes:
                if host not in current_hosts:
                    max_id += 1
                    config['members'].append(
                        {
                            '_id': max_id,
                            'host': host
                        }
                    )
                    any_change = True
            for member in config['members'][:]:
                if member['host'] not in self.nodes:
                    config['members'].remove(member)
                    any_change = True
            # Reconfig here replSetReconfig
            if any_change:
                try:
                    config['version'] += 1
                    self.connection.admin.command('replSetReconfig', config)
                except  Exception as fail_reason:
                    self.module.fail_json(
                            msg='Replicaset reconfig failed: {}'.format(fail_reason)
                            )
                else:
                    self.module.exit_json(
                        changed=True,
                        msg='ReplicaSet {} reconfigured'.format(
                            self.module.params['replset_name']
                            )
                        )
            else:
                self.module.exit_json(
                    changed=any_change,
                )
        else:
            members = [
            {'_id': num, 'host': node} for num, node in enumerate(self.nodes)
            ]
            '''
            for example members looks like
            [
                        {'_id': 0, 'host': '10.33.44.3'},
                        {'_id': 1, 'host': '10.33.44.120'},
                        {'_id': 2, 'host': '10.33.44.122'}
                        ]
            '''
            config = {
                '_id': self.name, 
                'members': members,
                    }
            with pymongo.MongoClient(list(self.nodes)[0]) as client:
                try:
                    # Random Host fore replicaset init
                    client.admin.command('replSetInitiate', config)
                except Exception as fail_reason:
                    if str(fail_reason) == 'already initialized':
                        self.module.exit_json(
                            changed=False,
                            msg='ReplicaSet {} already initiated'.format(
                                self.module.params['replset_name']
                            )
                        )
                    else:
                        self.module.fail_json(
                            msg='Replicaset initiation failed: {}'.format(fail_reason)
                            )
                else:
                    self.module.exit_json(
                        changed=True,
                        msg='ReplicaSet {} initiated'.format(
                            self.module.params['replset_name']
                            )
                        )
                    time.sleep(5)

    @staticmethod
    def str_list_to_list(string):
        '''
        Ugly, but work
        '''
        bad_symb = "'[] "
        cleared = ''.join(x for x in string if x not in bad_symb)
        return cleared.split(',')


def main():
    module = AnsibleModule(
        argument_spec = dict(
            members = dict(required=True),
            replset_name = dict(required=True),
            admin_login = dict(required=False),
            admin_password = dict(required=False)
        )
    )
    try:
        repl_set = MongoReplicaset(module)
        repl_set.sync_config()
    except Exception as error:
        module.fail_json(msg='{}'.format(error))



if __name__ == '__main__':
    main()
